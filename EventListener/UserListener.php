<?php
namespace Infinity\UserBundle\EventListener;

use Infinity\UserBundle\Event\UserEvent;
use Infinity\UserBundle\Model\UserInterface;
use Infinity\UserBundle\Services\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class UserListener implements EventSubscriberInterface
{
    /** @var EncoderFactoryInterface */
    private $encoderFactory;

    /** @var UserManagerInterface */
    private $userManager;

    /**
     * UserListener constructor.
     *
     * @param EncoderFactoryInterface $encoderFactory
     * @param UserManagerInterface    $userManager
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, UserManagerInterface $userManager)
    {
        $this->encoderFactory = $encoderFactory;
        $this->userManager = $userManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::SET_PASSWORD => 'setPassword',
            UserEvent::AUTHENTICATION_SUCCESS => 'authenticationSuccess',
        ];
    }

    /**
     * Generates a new encrypted password from plain text one.
     *
     * @param UserEvent $event
     */
    public function setPassword(UserEvent $event)
    {
        /** @var UserInterface $user */
        $user = $event->getUser();
        if (null !== $user->getPlainTextPassword()) {
            $user->setSalt($this->userManager->generateSalt());
            $user->setPassword(
                $this
                    ->encoderFactory
                    ->getEncoder($user)
                    ->encodePassword($user->getPlainTextPassword(), $user->getSalt())
            );
        }
    }

    /**
     * @param UserEvent $event
     */
    public function authenticationSuccess(UserEvent $event)
    {
        $this->userManager->registerAuthentication($event->getUser());
    }
}

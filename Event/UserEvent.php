<?php

namespace Infinity\UserBundle\Event;

use Infinity\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class UserEvent extends Event
{
    const SET_PASSWORD = 'infinity_user.set_password';
    const AUTHENTICATION_SUCCESS = 'infinity_user.authentication_success';
    const AUTHENTICATION_FAILURE = 'infinity_user.authentication_failure';

    /** @var UserInterface */
    private $user;

    /**
     * UserEvent constructor.
     *
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}

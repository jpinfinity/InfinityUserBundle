<?php
namespace Infinity\UserBundle\Doctrine;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Infinity\UserBundle\Model\AbstractUser;
use Infinity\UserBundle\Model\UserInterface;

class UserSubscriber implements EventSubscriber
{
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist => 'prePersist',
            Events::preUpdate  => 'preUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var AbstractUser $user */
        $user = $args->getEntity();
        if ($user instanceof UserInterface) {
            if (!$user->getCreatedAt()) {
                $user->setCreatedAt(new \DateTime());
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        /** @var AbstractUser $user */
        $user = $args->getEntity();
        if ($user instanceof UserInterface) {
            $user->setUpdatedAt(new \DateTime());
        }
    }
}

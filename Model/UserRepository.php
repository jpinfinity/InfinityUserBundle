<?php

namespace Infinity\UserBundle\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package Infinity\UserBundle\Model
 */
class UserRepository extends EntityRepository
{
    /**
     * @param string $username The username to find
     *
     * @return null|object
     */
    public function findOneByUsername($username)
    {
        return $this->findOneBy(array('username'=>$username));
    }
}

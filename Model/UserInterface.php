<?php

namespace Infinity\UserBundle\Model;

/**
 * Interface UserInterface
 * @package Infinity\UserBundle\Model
 */
interface UserInterface extends \Symfony\Component\Security\Core\User\UserInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @return string
     */
    public function getPlainTextPassword();

    /**
     * @param string $plainTextPassword
     * @return UserInterface
     */
    public function setPlainTextPassword($plainTextPassword);

    /**
     * @param string $salt
     * @return UserInterface
     */
    public function setSalt($salt);

    /**
     * @param string $password
     * @return UserInterface
     */
    public function setPassword($password);

    /**
     * @param \DateTime $lastLoginAt
     * @return mixed
     */
    public function setLastLoginAt(\DateTime $lastLoginAt);
}

<?php
namespace Infinity\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Infinity\UserBundle\Event\UserEvent;
use Infinity\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserManager implements UserManagerInterface
{
    /** @var EntityManager */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * UserManager constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param UserInterface $user
     */
    public function createUser(UserInterface $user)
    {
        $this->eventDispatcher->dispatch(UserEvent::SET_PASSWORD, new UserEvent($user));
    }

    /**
     * @param UserInterface $user
     */
    public function updateUser(UserInterface $user)
    {
        $plainTextPassword = $user->getPlainTextPassword();
        if (false === empty($plainTextPassword)) {
            $this->eventDispatcher->dispatch(UserEvent::SET_PASSWORD, new UserEvent($user));
        }
    }

    /**
     * @param UserInterface $user
     */
    public function registerAuthentication(UserInterface $user)
    {
        $user->setLastLoginAt(new \DateTime());
        $this->em->flush();
    }

    /**
     * @return string
     */
    public function generateSalt()
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }
}

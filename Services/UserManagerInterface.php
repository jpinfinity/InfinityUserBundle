<?php
namespace Infinity\UserBundle\Services;

use Infinity\UserBundle\Model\UserInterface;

interface UserManagerInterface
{
    /**
     * Generates a new salt chain
     *
     * @return string
     */
    public function generateSalt();

    public function createUser(UserInterface $user);

    public function updateUser(UserInterface $user);

    public function registerAuthentication(UserInterface $user);
}

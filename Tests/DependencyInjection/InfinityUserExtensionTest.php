<?php

namespace Infinity\UserBundle\Tests\DependencyInjection;

use Infinity\UserBundle\DependencyInjection\InfinityUserExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Parser;

class InfinityUserExtensionTest extends \PHPUnit_Framework_TestCase
{
    /** @var ContainerBuilder */
    protected $config;

    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     */
    public function testUserLoadThrowsExceptionUnlessDatabaseDriverSet()
    {
        $loader = new InfinityUserExtension();
        $config = $this->getConfig();
        unset($config['infinity_user']['password_encoder']);
        $loader->load([], new ContainerBuilder());
    }

    private function getConfig()
    {
        $yml = <<<EOF
infinity_user:
    password_encoder:
        pattern: "salt_[password]"
        algorithm: sha512
EOF;

        $parser = new Parser();
        return $parser->parse($yml);
    }
}

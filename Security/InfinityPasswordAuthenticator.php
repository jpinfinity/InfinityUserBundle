<?php
namespace Infinity\UserBundle\Security;

use Infinity\UserBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class InfinityPasswordAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    /** @var string */
    private $loginUrl = null;

    /** @var EncoderFactoryInterface */
    private $encoderFactory = null;

    /** @var EventDispatcherInterface */
    private $eventDispatcher = null;

    /**
     * Default message for authentication failure.
     *
     * @var string
     */
    private $failMessage = 'Invalid credentials. Please retry.';

    /**
     * InfinityPasswordAuthenticator constructor.
     *
     * @param RequestStack             $requestStack
     * @param EncoderFactoryInterface  $encoderFactory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        RequestStack $requestStack,
        EncoderFactoryInterface $encoderFactory,
        EventDispatcherInterface $eventDispatcher
    ) {
        if ($requestStack->getCurrentRequest() instanceof Request) {
            $this->loginUrl = $requestStack->getCurrentRequest()->headers->get('referer');
        }
        $this->encoderFactory = $encoderFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return array|string
     */
    protected function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * @inheritdoc
     */
    public function getCredentials(Request $request)
    {
        if ($request->request->has('_username')) {
            return [
                'username' => $request->request->get('_username'),
                'password' => $request->request->get('_password'),
            ];
        } else {
            return;
        }
    }

    /**
     * @inheritdoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $user = $userProvider->loadUserByUsername($credentials['username']);

            return $user;
        } catch (UsernameNotFoundException $e) {
            throw new CustomUserMessageAuthenticationException($this->failMessage);
        }
    }

    /**
     * @inheritdoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if ($this
            ->encoderFactory
            ->getEncoder($user)
            ->isPasswordValid($user->getPassword(), $credentials['password'], $user->getSalt())
        ) {
            return true;
        } else {
            throw new CustomUserMessageAuthenticationException($this->failMessage);
        }
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey
     *
     * @return null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $this->eventDispatcher->dispatch(UserEvent::AUTHENTICATION_SUCCESS, new UserEvent($token->getUser()));

        return null;
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->eventDispatcher->dispatch(UserEvent::AUTHENTICATION_FAILURE);

        return parent::onAuthenticationFailure($request, $exception);
    }
}

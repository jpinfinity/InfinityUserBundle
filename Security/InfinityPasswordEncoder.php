<?php
/**
 * User: JP Infinity <contact@julien-peltier.fr>
 * Created: 03/10/16 - 22:56
 */

namespace Infinity\UserBundle\Security;

use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class InfinityPasswordEncoder extends BasePasswordEncoder
{
    /** @var string */
    private $passwordPattern = null;

    /** @var string */
    private $algorithm;

    /** @var string */
    //private $allowDecrypt;

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->passwordPattern = isset($config['pattern']) ? $config['pattern'] : null;
        $this->algorithm = $config['algorithm'];
    }

    /**
     * @param string $raw
     * @param string $salt
     * @return string
     */
    public function encodePassword($raw, $salt)
    {
        if ($this->isPasswordTooLong($raw)) {
            throw new BadCredentialsException('Invalid password.');
        }

        $salted = $this->mergePasswordAndSalt($raw, $salt);
        $digest = hash($this->algorithm, $salted);
        return $digest;
    }

    /**
     * @param string $encoded
     * @param string $raw
     * @param string $salt
     * @return bool
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $this->encodePassword($raw, $salt) === $encoded;
    }

    /**
     * @param string $password
     * @param string $salt
     * @return mixed|string
     */
    protected function mergePasswordAndSalt($password, $salt)
    {
        if(empty($this->passwordPattern)){

            return parent::mergePasswordAndSalt($password, $salt);
        }
        else{
            $salted = $this->passwordPattern;
            $salted = preg_replace_callback(
                "`(salt|password)`",
                function($k) use($password, $salt){
                    $key = $k[1];

                    return $$key;
                },
                $salted);

            return $salted;
        }
    }
}

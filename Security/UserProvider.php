<?php
namespace Infinity\UserBundle\Security;

use Infinity\UserBundle\Services\UserManagerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    /**
     * @param string $username
     *
     * @return null|object
     */
    public function loadUserByUsername($username)
    {
        $user = $this->getRepository()->findOneBy(['username'=>$username]);

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        if (false === $this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(
                sprintf('User class "%s" not supported. Must implement UserInterface.', get_class($user))
            );
        }
        $user = $this->getRepository()->find(['id' => $user->getId()]);
        if (null === $user) {
            throw new UsernameNotFoundException(
                sprintf('User #%d could not be refreshed.', $user->getId())
            );
        }

        return $user;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        $userClass = $this->getClass();

        return $userClass === $class || is_subclass_of($class, $userClass);
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|UserRepository
     */
    public function getRepository()
    {
        if (empty($this->repository)) {
            $this->repository = $this->em->getRepository($this->userClass);
        }

        return $this->repository;
    }
}
